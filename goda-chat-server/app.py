from fastapi import FastAPI, HTTPException, Request, Form, UploadFile, File
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional
import json
import pandas as pd
from pydantic import BaseModel
from answer import answering 
from insert import insert_documents, process_data

app = FastAPI()

# Cấu hình CORS
origins = [
    "http://localhost:3000",  # Adjust to the frontend's URL
    "http://localhost:8080",  # Adjust to the frontend's URL
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class ChatRequest(BaseModel):
    content: str
    session_id: str
    longitude: float
    latitude: float

@app.post("/chat")
async def chat(chat_request: ChatRequest):
    try:
        response = await answering(
            chat_request.content,
            chat_request.session_id,
            chat_request.longitude,
            chat_request.latitude
        )
        return JSONResponse(content={"response": response})
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile = File(...)):
    if not file.filename.endswith('.json'):
        raise HTTPException(status_code=400, detail="Invalid file format. Please upload a .json file.")

    content = await file.read()
    try:
        data = json.loads(content)
    except json.JSONDecodeError:
        raise HTTPException(status_code=400, detail="Invalid JSON content.")

    if not isinstance(data, list):
        raise HTTPException(status_code=400, detail="Invalid JSON format. The JSON should contain a list of entries.")

    data_df = pd.DataFrame(data)
    docs = process_data(data_df)
    insert_documents(docs)

    return {"message": f"Processed {len(docs)} documents and inserted into MongoDB."}

@app.get("/")
def hello():
    return {"message": "Hello"}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

import os
import json
import pandas as pd
from pymongo import MongoClient
from langchain_openai import ChatOpenAI
from langchain.vectorstores import MongoDBAtlasVectorSearch
from langchain.embeddings import OpenAIEmbeddings
from langchain.docstore.document import Document
from answer import DB_NAME, LOCATION_COLLECTION_NAME, MONGO_URI
os.environ['OPENAI_API_KEY'] = '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'

# Initialize the ChatOpenAI and MongoClient
llm = ChatOpenAI(model_name="gpt-3.5-turbo-16k-0613")
client = MongoClient(MONGO_URI)
db = client['goda']

def config_bh(business_hours):
    """Process business hours data."""
    business_hours_str = str(business_hours)
    business_hours_str = business_hours_str.replace('{', '').replace('}', '').replace("'", '')
    return business_hours_str if business_hours_str else 'no information'

def config_review(reviews):
    """Summarize reviews focusing only on the 'review_content'."""
    try:
        review_texts = [review['review_content'] for review in reviews if review['review_content'].strip()]
        review_texts_limited = review_texts[:10] 
        reviews_combined = '\n'.join(f"Review {i + 1}: {rv}" for i, rv in enumerate(review_texts_limited))
        if reviews_combined:
            summary = llm.invoke(f"Tóm tắt lại thành 1 đoạn đánh giá: {reviews_combined}").content
            print("sumary"+ summary)
            return summary
        else:
            return 'No reviews to summarize.'
    except Exception as e:
        print(f"Error summarizing reviews: {e}")
        return 'no information'

def process_data(data):
    """Process and prepare documents for insertion, skipping entries without a 'name'."""
    docs = []
    for _, row in data.iterrows():
        if 'name' in row and row.get('name'):
            location_array = row.get('location', [None, None])
            latitude, longitude = float(location_array[0]), float(location_array[1])
            metadata = {
                'name': row['name'],
                'address': row.get('address', ''),
                'business_hours': row.get('business_hours', {}),
                'phone_number': row.get('phone_number', ''),
                'avg_rate': row.get('rate', 'Không có đánh giá'),
                'reviews': row.get('reviews', []),
                'image_link': row.get('photo_link', '')[0],
                'content': row.get('content', ''),
                'menu': row.get('menu', ''),
                'location': {
                    'type': 'Point',
                    'coordinates': [longitude, latitude] 
                },
                'type': row.get('type'),
                'url_page': row.get('url_page'),
                'sum_price': row.get('sum_price',"Không có thông tin"),
            }
            info = f"Summary: {metadata['name']},Mô tả {metadata['content']},Menu các món :{metadata['menu']},Giá trung bình : {metadata['sum_price']} Đánh giá {metadata['avg_rate']} sao,Địa chỉ {metadata['address']}, Số điện thoại: {metadata['phone_number']}, Giờ mở cửa: {config_bh(metadata.get('business_hours', {}))}"
            print(info)
            doc = Document(page_content=info, metadata=metadata)
            docs.append(doc)
        else:
            print("Skipping document without a 'name'")
    return docs


def insert_documents(docs, collection_name):
    """Insert documents into MongoDB."""
    collection = db[collection_name]
    vector_store = MongoDBAtlasVectorSearch(
        collection=collection,
        embedding=OpenAIEmbeddings(disallowed_special=()),
        index_name="vector_index"
    )
    vector_store.add_documents(docs)

def insert(folder='data/'):
    """Main function to process and insert documents from JSON files."""
    for f in os.listdir(folder):
        file_path = os.path.join(folder, f)
        with open(file_path, 'r', encoding='utf-8') as file:
            data_json = json.load(file)
            data_df = pd.DataFrame(data_json)
            docs = process_data(data_df)
            insert_documents(docs, 'data_location')
            print(f"Processed and inserted documents from {file_path}")

if __name__ == "__main__":
    insert()

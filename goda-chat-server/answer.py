import os
import pandas as pd
import datetime
from pymongo import MongoClient
from langchain_openai import ChatOpenAI
from langchain.vectorstores import MongoDBAtlasVectorSearch
from langchain.embeddings import OpenAIEmbeddings
import re
import json
from motor.motor_asyncio import AsyncIOMotorClient
from math import radians, sin, cos, sqrt, atan2

# Set environment variables
# os.environ['OPENAI_API_KEY'] = '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'
os.environ['OPENAI_API_KEY'] = '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'
# MongoDB settings
MONGO_URI = 'mongodb+srv://goda:goda@goda.jtbvkfx.mongodb.net/?retryWrites=true&w=majority'
DB_NAME = 'goda'
LOCATION_COLLECTION_NAME = 'data_location'
CHAT_COLLECTION_NAME = 'message_history'
CACHE_COLLECTION_NAME = 'answer_cache'  # New collection for caching answers

# Initialize ChatOpenAI and MongoClient
llm = ChatOpenAI(model_name="gpt-3.5-turbo-16k-0613")
client = AsyncIOMotorClient(MONGO_URI)
db = client[DB_NAME]
location_collection = db[LOCATION_COLLECTION_NAME]
chat_collection = db[CHAT_COLLECTION_NAME]
cache_collection = db[CACHE_COLLECTION_NAME]  # Initialize cache collection

# Initialize MongoDBAtlasVectorSearch for main location data
vector_search = MongoDBAtlasVectorSearch.from_connection_string(
    MONGO_URI,
    DB_NAME + "." + LOCATION_COLLECTION_NAME,
    OpenAIEmbeddings(disallowed_special=()),
    index_name="vector_index"
)

async def add_user_message(session_id, user_message):
    message_doc = {
        'session_id': session_id,
        'content': user_message,
        'author': 'USER',
        'timestamp': datetime.datetime.utcnow()
    }
    await chat_collection.insert_one(message_doc)
def haversine_distance(lat1, lon1, lat2, lon2):
    R = 6371  # Radius of the Earth in kilometers
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])

    # Haversine formula
    dlat = lat2 - lat1
    dlon = lon2 - lon1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    # Calculate the distance
    distance = R * c
    return distance

def is_within_radius(current_lat, current_lon, shop_lat, shop_lon, radius):
    distance = haversine_distance(current_lat, current_lon, shop_lat, shop_lon)
    return distance <= radius
async def add_ai_message(session_id, ai_message):
    message_doc = {
        'session_id': session_id,
        'content': ai_message,
        'author': 'GODA',
        'timestamp': datetime.datetime.utcnow()
    }
    await chat_collection.insert_one(message_doc)

async def get_past_messages(session_id, limit=10):
    query = {'session_id': session_id}
    messages = await chat_collection.find(query).sort('timestamp', -1).to_list(length=limit)
    messages = messages[::-1]  # Reverse to maintain chronological order
    return messages
async def get_context_for_answering(query, longitude, latitude):
    data = vector_search.similarity_search(
        query=query,
        k=10
    )
    # for d in data:
    #     location_longitude,location_latitude =  d.metadata['location']['coordinates']
    #     print(location_longitude,location_latitude)
    #     if is_within_radius(longitude, latitude, location_longitude, location_latitude, 20):
    #         context.append(d.page_content)
    # print("sdsdgsdg")
    # context = [d.page_content for d in data]
    # print("sdsdgsdg")
    context = [d.page_content for d in data]
    return '\n'.join(context)

async def answering(query, session_id, longitude, latitude):
    query_str = query.encode().decode("UTF-8")
    
    await add_user_message(session_id, query_str)
    
    context = await get_context_for_answering(query_str, longitude, latitude)
    past_messages = await get_past_messages(session_id, limit=10)
    past_messages_text = '\n'.join(msg['content'] for msg in past_messages)
    combined_context = f"Trong những cuộc trò chuyện trước, chúng ta đã thảo luận về các địa điểm sau (nếu có): {past_messages_text}. Với câu hỏi hiện tại, thông tin liên quan bao gồm: {context}."
    prompt = f"""
Bạn là GODA AI, một hướng dẫn viên du lịch ảo gen z với kiến thức sâu rộng về du lịch Thạch Thất, Hà Nội, Việt Nam. Sử dụng thông tin từ "{combined_context}" để trả lời câu hỏi: "{query_str}".
Yêu cầu:
-Tư vấn tận tình người hỏi , gợi ý các lựa chọn cho người hỏi
- Nếu câu hỏi  chung chung, hãy yêu cầu thông tin cụ thể hơn , không được trả về địa điểm mà phải hỏi kĩ hơn thì mới được cung cấp.
- Cung cấp thông tin chính xác, tar lời ngắn gọn  và hấp dẫn về tối đa 5 địa điểm, mỗi địa điểm một câu giới thiệu .
- Khi không có đủ thông tin, hãy yêu cầu làm rõ hoặc đề xuất các địa điểm phổ biến hoặc được đánh giá cao ưu tiên 4 đến 5 sao.
- Với mỗi địa địa điểm hãy tách ra thành các đoạn xuống dòng để dễ nhìn , tên địa điểm in đậm
- câu trả lời phải có liên hệ với các cuộc hội thoại trước đó
- Định dạng tên địa điểm trong dấu ngoặc kép, in đậm và cung cấp thông tin dữ liệu không có nếu bạn biết nếu có.
"""
    answer = llm.invoke(prompt).content
    
    await add_ai_message(session_id, answer)

    place_names = re.findall(r"\"(.*?)\"", answer)
    relevant_data = []
    for name in place_names:
        data = await location_collection.find_one({"name": name})
        if data:
            print(data)
            formatted_data = {
                'id': str(data['_id']),
                'name': data.get('name', ''),
                'image_link': data.get('image_link', ''),
                'location': data.get('location', []),
                'avg_rate': data.get('avg_rate', []),
            }
            relevant_data.append(formatted_data)
    
    response = {
        'answer': answer,
        'places': json.dumps(relevant_data, ensure_ascii=False)
    }
    
    return response
import express from 'express';
import connectToDatabase from './src/config/db.js';
import dotenv from 'dotenv';
import router from "./src/routes/route.js"
dotenv.config()
const app = express();
app.use(express.json()); 
const main = async ()=>{
    await connectToDatabase();
}
main();
app.use('/api', router);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

// import { MongoClient, ServerApiVersion } from  'mongodb';
// const uri = "mongodb+srv://goda:goda@goda.emp1dsb.mongodb.net/?retryWrites=true&w=majority";

// // Create a MongoClient with a MongoClientOptions object to set the Stable API version
// const client = new MongoClient(uri, {
//   serverApi: {
//     version: ServerApiVersion.v1,
//     strict: true,
//     deprecationErrors: true,
//   }
// });

// async function run() {
//   try {
//     // Connect the client to the server	(optional starting in v4.7)
//     await client.connect();
//     // Send a ping to confirm a successful connection
//     await client.db("test").command({ ping: 1 });
//     console.log("Pinged your deployment. You successfully connected to MongoDB!");
//   } finally {
//     await client.close();
//   }
// }
// run().catch(console.dir);

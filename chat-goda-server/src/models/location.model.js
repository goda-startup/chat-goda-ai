import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const businessHoursSchema = new Schema({
    Saturday: String,
    Sunday: String,
    Monday: String,
    Tuesday: String,
    Wednesday: String,
    Thursday: String,
    Friday: String
}); 
const reviewSchema = new Schema({
    name:  String,
    review_time: String, 
    rating: String,
    review_content: String,
});

const locationSchema = new Schema({
    name: { type: String, required: true },
    address: String,
    business_hours: businessHoursSchema,
    phone_number: String,
    photo_link: String,
    content: String,
    rate: String,
    reviews: [reviewSchema],
    type: String,
    location: {
        type: [Number],
    },
    metadata: String
});

export const Location = mongoose.model('Location', locationSchema);

// route.js
import express from 'express';
import multer from 'multer';
import { saveLocationsToDatabase } from '../controllers/locationController.js'; 


const router = express.Router();

// Configure Multer to handle JSON files
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (file.mimetype === 'application/json') {
      cb(null, true);
    } else {
      cb(new Error('Only JSON files are allowed!'), false);
    }
  },
});

router.post('/upload', upload.single('jsonFile'), async (req, res) => {
  try {
    // Check if req.file is defined
    if (!req.file) {
      throw new Error('No file uploaded.');
    }

    const jsonData = JSON.parse(req.file.buffer.toString('utf8'));

    // Use the saveLocationsToDatabase function to process and save data
    await saveLocationsToDatabase(jsonData);

    res.status(200).json({ message: 'Data has been uploaded and saved to the database.' });
  } catch (error) {
    console.error('Error processing JSON file:', error);
    res.status(500).json({ error: 'An error occurred while processing the JSON file.' });
  }
});

export default router;

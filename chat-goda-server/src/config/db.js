import { MongoClient } from 'mongodb';
import { config } from './env.js';

const connectToDatabase = async () => {
    const client = new MongoClient(config.MONGODB_ATLAS_URI, {
        useNewUrlParser: true, 
        useUnifiedTopology: true, 
        connectTimeoutMS: 30000,
        socketTimeoutMS: 45000,
    });

    try {
        await client.connect();
        console.log("Connected to MongoDB");
        const db = client.db('test');
        db.collection('locations');
    } catch (error) {
        console.error("Error connecting to MongoDB:", error);
    }
};

export default connectToDatabase;

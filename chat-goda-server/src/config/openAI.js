import { OpenAI ,OpenAIEmbeddings } from "@langchain/openai";
import { config } from "./env.js";
  const modelGpt = new OpenAI({
    modelName: config.MODEL_LANGUAGE,
    openAIApiKey: config.OPEN_AI_KEY,
  });
  const embeddings = new OpenAIEmbeddings({
    openAIApiKey: config.OPEN_AI_KEY, 
    batchSize: 512,
  });
  const sumReview = async (list) => {
    try {
        const data = list.map(item => item.review_content);
        let rvs = '';
        data.forEach((rv, i) => {
            if (rv !== '') {
                rvs += `review ${i + 1}: ${rv}\n`;
            }
        });
        const result = modelGpt.call(`${rvs} 
            only answer my question, do not give more information, sort and accurate
            summary all the reviews above less than 100 words.
        `);
        return result;
    } catch (error) {
        return 'no information';
    }
}
export { sumReview ,embeddings};

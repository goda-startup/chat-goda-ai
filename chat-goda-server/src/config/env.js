import dotenv from 'dotenv'
dotenv.config()
export const config = {
    MONGODB_ATLAS_URI: process.env.MONGODB_ATLAS_URI ,
    OPEN_AI_KEY: process.env.OPEN_AI_KEY ,
    MODEL_LANGUAGE: process.env.MODEL_LANGUAGE ,
  };
import mongoose from 'mongoose';
import { Location } from '../models/location.model.js'; 
import { sumReview } from '../config/openAI.js';
const customizeData = async (locationData) => {
    try {
        if (!locationData.name) {
            throw new Error('Location data must have a name.');
        }
        if (locationData.rate === "") {
            locationData.rate = "N/A";
        }
        // locationData.metadata = await sumReview(locationData.reviews);
        return locationData;
    } catch (error) {
        console.error('Error customizing location data:', error);
        throw error; 
    }
};

const saveLocationsToDatabase = async (jsonData) => {
    try {
        const customizedData = await Promise.all(jsonData.map(customizeData));
        await Location.create(customizedData);
    } catch (error) {
        console.error('Error saving locations to the database:', error);
        throw error; 
    }
};

export { saveLocationsToDatabase };

import { sumReview } from "../config/openAI.js";
import { embeddings } from "../config/openAI.js";
const createEmbeddings = async (location) => {
    const textToEmbed = [
        location.name,
        location.address,
        location.content,
        ... await sumReview(location.reviews)
    ].join(' ');

    try {
        const embeddingResult = await embeddings.embedQuery(textToEmbed); 
        return embeddingResult;
    } catch (error) {
        console.error('Failed to create embeddings:', error);
        throw error;
    }
};
export {createEmbeddings}